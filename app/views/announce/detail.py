from django.views.generic import DetailView

from app.models import Announce


class AnnounceDetailView(DetailView):
    template_name = '../templates/announce_detail.html'
    model = Announce

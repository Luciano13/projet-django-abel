from django.views.generic import ListView

from app.models import Announce


class AnnounceListView(ListView):
    template_name = '../templates/announce_list.html'
    model = Announce


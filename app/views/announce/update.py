from django.urls import reverse
from django.views.generic import UpdateView

from app.forms.announce import AnnounceForm
from app.models import Announce


class AnnounceUpdateView(UpdateView):
    template_name = '../templates/announce_update.html'
    model = Announce
    form_class = AnnounceForm

    def get_success_url(self):
        return reverse('app_index')
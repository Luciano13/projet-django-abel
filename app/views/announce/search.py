from functools import reduce

from django.db.models import Q
from django.views.generic import ListView
import operator

from app.models import Announce


class SearchResultsView(ListView):
    template_name = '../templates/search_results.html'
    model = Announce

    paginate_by = 10

    def get_queryset(self):
        result = super(SearchResultsView, self).get_queryset()

        q = self.request.GET.get('q')
        if q :
            result = result.filter(Q(title__icontains=q) | Q(description__icontains=q))
        return result

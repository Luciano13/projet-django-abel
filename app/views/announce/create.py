from django.urls import reverse
from django.views.generic import CreateView

from app.forms.announce import AnnounceForm
from app.models import Announce


class AnnounceCreateView(CreateView):
    template_name = '../templates/announce_create.html'
    model = Announce
    form_class = AnnounceForm

    def get_success_url(self):
        return reverse('list_announce')

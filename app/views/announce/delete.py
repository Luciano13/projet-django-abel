from django.urls import reverse
from django.views.generic import DeleteView

from app.models import Announce


class AnnounceDeleteView(DeleteView):
    template_name = '../templates/announce_delete.html'
    model = Announce

    def get_success_url(self):
        return reverse('app_index')

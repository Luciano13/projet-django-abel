from django.views.generic import ListView

from app.models import Announce


class IndexView(ListView):
    template_name = '../templates/index.html'
    model = Announce

    def get_context_data(self, **kwargs):
        result = super().get_context_data(**kwargs)
        result['title'] = 'Achètes & Vends facilement !'
        return result

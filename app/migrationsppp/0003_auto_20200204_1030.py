# Generated by Django 2.2.10 on 2020-02-04 09:30

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0002_auto_20200203_1122'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='announce',
            options={'ordering': ['-created_at']},
        ),
        migrations.AlterField(
            model_name='announce',
            name='picture',
            field=models.ImageField(blank=True, upload_to='images'),
        ),
    ]

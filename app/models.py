from django.conf import settings
from django.contrib.auth.models import User
from django.db import models


class Announce(models.Model):
    title = models.CharField(max_length=80)
    description = models.TextField(blank=True, null=True)
    price = models.FloatField(default=1.0)
    location = models.CharField(max_length=80)
    picture = models.ImageField(upload_to='images', blank=True)
    categories = models.ManyToManyField("Category")
    validation = models.BooleanField(default=False, null=True)
    author = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    update_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ["-created_at"]

    def __str__(self):
        description = f'{self.description[:80]}...' if len(self.description) > 80 else self.description
        return '[{}] {} ({})'.format(self.title, description,
                                ' / '.join([str(c) for c in self.categories.all()]))


class Category(models.Model):
    name = models.CharField(max_length=200)
    group_category = models.ForeignKey("GroupCategory", on_delete=models.CASCADE)

    def __str__(self):
        return str(self.name)


class GroupCategory(models.Model):
    name = models.CharField(max_length=200)

    def __str__(self):
        return str(self.name)

#
# class Profile(models.Model):
#     user = models.OneToOneField(User)  # La liaison OneToOne vers le modèle User
#     site_web = models.URLField(blank=True)
#     avatar = models.ImageField(null=True, blank=True, upload_to="avatars/")
#     signature = models.TextField(blank=True)
#     inscrit_newsletter = models.BooleanField(default=False)
#
#     def __str__(self):
#         return "Profil de {0}".format(self.user.username)
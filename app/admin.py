from django.contrib import admin

from app.models import Announce, Category, GroupCategory

admin.site.register(Announce)
admin.site.register(Category)
admin.site.register(GroupCategory)

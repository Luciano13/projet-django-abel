from django.forms import models

from app.models import Announce


class AnnounceForm(models.ModelForm):
    class Meta:
        model = Announce
        fields = ['title', 'description', 'price', 'location', 'picture', 'categories']


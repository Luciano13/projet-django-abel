"""LBC URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.contrib.auth.views import LoginView

from app.views.announce.signup import SignUp_View
from . import settings
from django.contrib.staticfiles.urls import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import path, include

from app.views.announce.create import AnnounceCreateView
from app.views.announce.delete import AnnounceDeleteView
from app.views.announce.detail import AnnounceDetailView
from app.views.announce.index import IndexView
from app.views.announce.list import AnnounceListView
from app.views.announce.search import SearchResultsView
from app.views.announce.update import AnnounceUpdateView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('accounts/', include('django.contrib.auth.urls')),
    path('signup/', SignUp_View, name="signup"),
    url(r'^$', IndexView.as_view(), name='app_index'),
    path('create/', AnnounceCreateView.as_view(), name='create_announce'),
    path('delete/<int:pk>', AnnounceDeleteView.as_view(), name='delete_announce'),
    path('detail/<int:pk>', AnnounceDetailView.as_view(), name='detail_announce'),
    path('update/<int:pk>', AnnounceUpdateView.as_view(), name='update_announce'),
    path('list/', AnnounceListView.as_view(), name='list_announce'),
    path('search/', SearchResultsView.as_view(), name='search_results'),
]

urlpatterns += staticfiles_urlpatterns()
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)